<!-- components/readme/usage.php -->
<p>Due to the changing nature of this process, the concept is to use these assets directly from their source as much as possible. </p>

For example:
<ul>
    <li>
    	<!-- Embed stylesheet link -->
        <label for="embedStyle"><strong>Embed the style sheet:</strong> </label>
        <input type="text" id="embedStyle" value="http://brand.interactivescientific.com/css/main.css" />
        <button data-copytarget="#embedStyle">copy link</button>
	</li>
    <li><strong>Link directly to logo files using the urls supplied</strong> (instead of downloading and reuploading)</li>
    <li><strong>When supplying assets to external partners </strong> link them directly to this resource instead of emailing assets</li>
</ul>