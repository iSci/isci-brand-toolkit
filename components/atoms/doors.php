<!-- components/atoms/doors.php -->

<!-- Door example plain -->
<div class="door-panel white alt">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
<path class="door" fill="#d9d9d9" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg>    
</div>

<!-- Door example plain -->
<div class="door-panel white dark">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
<path class="door" fill="#0d0d0d" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg>    
</div>

