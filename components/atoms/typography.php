<!-- components/atoms/typography.php -->

<span>Base Typography</span>
<h2 class="font font1">
    <span>Source Sans Pro</span>
    <span>$font1</span>
</h2>

<span>Logo Typography</span>
<h2 class="font logofont">
    <span>Galano Grotesque DEMO Bold</span>
    <span>$logo</span>
</h2>

<span>Logo Alternative Typography</span>
<h2 class="font logofontalt">
    <span>Galano Grotesque Alt DEMO Bold</span>
    <span>$logoalt</span>
</h2>

<span>Alternative Typography</span>
<h2 class="font fontalt1">
    <span>Source Serif Pro</span>
    <span>$fontalt1</span>
</h2>

<h2 class="font fontalt2">
    <span>Roboto</span>
    <span>$fontalt2</span>
</h2>

<!-- List of download links -->
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-download"></span></div>
    <ul class="onclick-menu-content">
        <li><a href="https://fonts.google.com/?selection.family=Source+Sans+Pro">Source Sans Pro</a></li>
        <li><a href="https://www.myfonts.com/fonts/rene-bieder/galano-grotesque/demo-bold/">Galano Grotesque Demo</a></li>
        <li><a href="https://www.myfonts.com/fonts/rene-bieder/galano-grotesque/alt-demo-bold/">Galano Grotesque Alt Demo</a></li>
        <li><a href="https://fonts.google.com/?selection.family=Source+Sans+Pro">Source Serif Pro</a></li>
        <li><a href="https://fonts.google.com/?selection.family=Roboto">Roboto</a></li>
    </ul>
</div>
