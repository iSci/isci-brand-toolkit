<!-- components/readme/about.php -->

<h3>Living Style Guide</h3>
<p>It is a work in progress, 
and currently is a starting point for the building blocks that 
make the Interactive Scientific brand toolkit. </p>
<p>The brand and this toolkit will grow with us. We continue to add and alter elements that make our brand. See <a href="atomic-core/?cat=readme#usage">usage</a> for more details.</p>

<h3>Atomic Design</h3>
<p>This style guide has adopted the <a href="http://bradfrost.com/blog/post/atomic-web-design/">
    Atomic Design</a> system by Brad Frost.</p>
    
<strong>What is atomic design?</strong>
<p>Atomic design is methodology for creating design systems. The three base levels in atomic design are:</p>
<uL>
    <li>Atoms</li>
    <li>Molecules</li>
    <li>Organisms</li>
</uL>

For more information on Atomic Design please read <a href="http://bradfrost.com/blog/post/atomic-web-design/">this article</a> by Brad Frost.