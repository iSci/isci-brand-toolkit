<!-- components/atoms/colour.php -->

<h2 class="font font1">Base Colours</h2>
<div class="colors">
    <div class="color purple">
        <span>.purple<br>#4b289b</span>
    </div>
    <div class="color orange">
        <span>.orange<br>#ffa100</span>
    </div>
     <div class="color blue">
        <span>.blue<br>#3CB0C7</span>
    </div>
     <div class="color green">
        <span>.green<br>#47a425</span>
    </div>
    <div class="color white">
        <span>.white<br>#fff</span>
    </div>
    <div class="color black">
        <span>.black<br>#000</span>
    </div>
</div>

<div class="colors">
    <div class="color purple light">
        <span>.purple .light<br>#7c56d3</span>
    </div>
    <div class="color orange light">
        <span>.orange .light<br>#ffbd4d</span>
    </div>
    <div class="color blue light">
        <span>.blue .light<br>#77c8d8</span>
    </div>
    <div class="color green light">
        <span>.green .light<br>#69d441</span>
    </div>
    <div class="color white alt">
        <span>.white .alt<br> #f2f2f2</span>
    </div>
    <div class="color black light"">
        <span>.black .light<br> #262626</span>
    </div>
</div>

<div class="colors">
    <div class="color purple dark">
        <span>.purple .dark<br>#24134a</span>
    </div>
    <div class="color orange dark">
        <span>.orange .dark<br>#b37100</span>
    </div>
    <div class="color blue dark">
        <span>.blue .dark<br>#297d8e</span>
    </div>
    <div class="color green dark">
        <span>.green .dark<br>#2c6617</span>
    </div>
    <div class="color white dark">
        <span>.white .dark<br> #d9d9d9</span>
    </div>
    <div class="color black alt"">
        <span>.black .alt<br> #0d0d0d</span>
    </div>
</div>
<br>
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-download"></span></div>
    <ul class="onclick-menu-content">
        <li><a href="assets/colours/iSci-colours.ase" download>Adobe Swatch Exchange (ASE)</a></li>
    </ul>
</div>