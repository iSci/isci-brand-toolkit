<!-- components/molecules/social.php -->

<!-- Avatar -->
 <h2>Avatars</h2>
 <p>Square size for Twitter should be usable on most social media platforms. <i>Click images to download png.</i></p>
<div class="fullwidth">
  <a href="/assets/templates/social/twitter/iSci_Twitter_Avatar_purple.png" download><img src="/assets/templates/social/twitter/iSci_Twitter_Avatar_purple.png" width="100px"></a>
  <a href="/assets/templates/social/twitter/iSci_Twitter_Avatar_purple_dark.png" download><img src="/assets/templates/social/twitter/iSci_Twitter_Avatar_purple_dark.png" width="100px"></a>
  <a href="/assets/templates/social/twitter/iSci_Twitter_Avatar_orange.png" download><img src="/assets/templates/social/twitter/iSci_Twitter_Avatar_orange.png" width="100px"></a>
  <a href="/assets/templates/social/twitter/iSci_Twitter_Avatar_orange_dark.png" download><img src="/assets/templates/social/twitter/iSci_Twitter_Avatar_orange_dark.png" width="100px"></a>
  <a href="/assets/templates/social/twitter/iSci_Twitter_Avatar_blue.png" download><img src="/assets/templates/social/twitter/iSci_Twitter_Avatar_blue.png" width="100px"></a>
  <a href="/assets/templates/social/twitter/iSci_Twitter_Avatar_blue_dark.png" download><img src="/assets/templates/social/twitter/iSci_Twitter_Avatar_blue_dark.png" width="100px"></a>
  <a href="/assets/templates/social/twitter/iSci_Twitter_Avatar_green.png" download><img src="/assets/templates/social/twitter/iSci_Twitter_Avatar_green.png" width="100px"></a>
  <a href="/assets/templates/social/twitter/iSci_Twitter_Avatar_green_dark.png" download><img src="/assets/templates/social/twitter/iSci_Twitter_Avatar_green_dark.png" width="100px"></a>
  <a href="/assets/templates/social/twitter/iSci_Twitter_Avatar_white.png" download><img src="/assets/templates/social/twitter/iSci_Twitter_Avatar_white.png" width="100px"></a>
  <a href="/assets/templates/social/twitter/iSci_Twitter_Avatar_mosaic.png" download><img src="/assets/templates/social/twitter/iSci_Twitter_Avatar_mosaic.png" width="100px"></a>

  <br>
  <!-- List of links -->
  <div tabindex="0" class="onclick-menu">
  	<h2>Avatar Templates</h2>
    <div class="circle"><span class="fa fa-download"></span></div>
    <ul class="onclick-menu-content">
      <li><a href="/assets/templates/social/Twitter_Avatar.psb" download>Twitter Avatar (PSB)</a></li>
    </ul>
  </div>
</div>

<!-- Cover -->
 <h2>Cover Images</h2>
  <p>Dimensions for Twitter should be usable on most social media platforms. <i>Click images to download png.</i></p>
<div class="fullwidth">
  <a href="/assets/templates/social/twitter/iSci_Twitter_Cover_orb.png" download><img src="/assets/templates/social/twitter/iSci_Twitter_Cover_orb.png" width="200px"></a>
  <a href="/assets/templates/social/twitter/iSci_Twitter_Cover_rainforest.png" download><img src="/assets/templates/social/twitter/iSci_Twitter_Cover_rainforest.png" width="200px"></a>
  <br>
  <!-- List of links -->
  <div tabindex="0" class="onclick-menu">
  	<h2>Cover Image Templates</h2>
    <div class="circle"><span class="fa fa-download"></span></div>
    <ul class="onclick-menu-content">
      <li><a href="/assets/templates/social/Twitter_Cover_Image.psb" download>Twitter Cover Image (PSB)</a></li>
    </ul>
  </div>
</div>