<!-- components/molecules/businessCard.php -->

<div class="fullwidth">
  <h2>Template: iSci Business Card</h2>
   <img src="/assets/templates/businessCards/example/isci_bcards.png" width="300px">
   <img src="/assets/templates/businessCards/example/isci_bcards2.png" width="300px">
   <br>
  <!-- List of links -->
	<div tabindex="0" class="onclick-menu">
	    <a href="/assets/templates/businessCards/isci_bcards.indt" download>
	    <div class="circle"><span class="fa fa-download"></span></div>
	    </a>
	</div>
</div>

<div class="fullwidth">
  <h2>Individuals: iSci Business Card</h2>
  <!-- List of links -->
	<div tabindex="0" class="onclick-menu">
            <div class="circle"><span class="fa fa-download"></span></div>
            <ul class="onclick-menu-content">
            	<li><a href="/assets/templates/businessCards/individuals/isci_bcards_individuals-pdf.zip" download>All Individuals (ZIPPED PDFs)</a></li>
                <li><a href="/assets/templates/businessCards/individuals/AlixBriskham_isci_bcard copy.indd" download>Alix Briskham (INDD)</a></li>
                <li><a href="/assets/templates/businessCards/individuals/BalazsHornung_isci_bcard.indd" download>Balazs Hornung (INDD)</a></li>

                <li><a href="/assets/templates/businessCards/individuals/BeckyDavies_isci_bcard.indd" download>Becky Davies (INDD)</a></li>
                <li><a href="/assets/templates/businessCards/individuals/BeckySage_isci_bcard.indd" download>Becky Sage (INDD)</a></li>
                <li><a href="/assets/templates/businessCards/individuals/DavidGlowacki_isci_bcard.indd" download>David Glowacki (INDD)</a></li>
                <li><a href="/assets/templates/businessCards/individuals/General_isci_bcard.indd" download>General (INDD)</a></li>
                <li><a href="/assets/templates/businessCards/individuals/JaneyMcLeod_isci_bcard.indd" download>Janey McLeod (INDD)</a></li>
                <li><a href="/assets/templates/businessCards/individuals/JennBlackwood_isci_bcard.indd" download>Jenn Blackwood (INDD)</a></li>
                <li><a href="/assets/templates/businessCards/individuals/LiamCullingford_isci_bcard.indd" download>Liam Cullingford (INDD)</a></li>

                <li><a href="/assets/templates/businessCards/individuals/LizRabone_isci_bcard.indd" download>Liz Rabone (INDD)</a></li>
                <li><a href="/assets/templates/businessCards/individuals/MarkWonnacott_isci_bcard.indd" download>Mark Wonnacott (INDD)</a></li>
                <li><a href="/assets/templates/businessCards/individuals/PhillTew_isci_bcard.indd" download>Phill Tew (INDD)</a></li>

            </ul>
        </div>
</div>