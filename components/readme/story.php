<!-- components/readme/story.php -->

<div class="brand ambition" id="grad-purple-dark">
    <svg class="isci-logo-white" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 789.3 643.5" style="enable-background:new 0 0 789.3 643.5;" xml:space="preserve">
<style type="text/css">
	.st0{fill:none;stroke:#000000;stroke-width:21.2598;}
	.st1{font-family:'galano_grotesque_demobold';}
	.st2{font-size:131.2294px;}
	.st3{font-size:131.23px;}
</style>
<path class="st0" d="M216.2,433.5v84.1L10.8,626.1v-609l205.4,108.4v70"/>
<g>
	<polygon points="0.2,643.7 0.2,-0.5 226.8,119.1 226.8,195.5 205.5,195.5 205.5,131.9 21.4,34.7 21.4,608.5 205.5,511.2 
		205.5,433.5 226.8,433.5 226.8,524.1 	"/>
</g>
<text transform="matrix(1 0 0 1 73.9134 301.5081)" class="st1 st2">interactive</text>
<text transform="matrix(0.9976 0 0 1 36.2591 413.4471)" class="st1 st3">Scientific</text>
</svg>

    <p>Our visual brand story</p>
    <h1>Everyone is welcome to come in: <span class="purple light"> transparent & accessible. </span> </h1>
    <h1><span class="purple light">Open the door</span> to everyday global challenges that affect us all. </h1>
    <h1>What do molecules look like? </h1>
    <h1>Abstract science concepts lead to learning experiences. </h1>
    <h1>A window frame for molecular aesthetics. </h1>
    <h1>Interactive Scientific: <span class="purple light">The Door to the Nano World</span></h1>
</div>