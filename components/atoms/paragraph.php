<!-- components/atoms/paragraph.php -->

<p>Interactive Scientific Ltd (iSci) is a multi-award-winning company which is dedicated to the development of transformative, tactile digital and analogue experiences that explore the scientific world.</p>