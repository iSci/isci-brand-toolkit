<!-- components/molecules/document.php -->

<div class="fullwidth">
  <h2>Templates: iSci Word Docs</h2>
  <p>Remember to download and install fonts before using templates. <a href="/atomic-core/?cat=atoms#typography">Go to Typography.</a></p>
  <!-- List of links -->
  <div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-download"></span></div>
    <ul class="onclick-menu-content">
      <li><a href="/assets/templates/documents/isci_blankDoc.dotx" download>Blank Document (DOTX)</a></li>
      <li><a href="/assets/templates/documents/isci_letterhead.dotx" download>Letterhead (DOTX)</a></li>
      <li><a href="/assets/templates/documents/isci_terms.dotx" download>Terms (DOTX)</a></li>
      <li><a href="/assets/templates/documents/isci_activityPlan.dotx" download>Activity Plan (DOTX)</a></li>
      <li><a href="/assets/templates/documents/isci_feedbackForm.dotx" download>Feedback Form (DOTX)</a></li>
      <li><a href="/assets/templates/documents/isci_userConsultation.dotx" download>User Consultation (DOTX)</a></li>
      <li><a href="/assets/templates/documents/isci_usabilityProblems.dotx" download>Usability Problems (DOTX)</a></li>
    </ul>
  </div>
</div>