<!-- components/molecules/letterhead.php -->

<div class="fullwidth">
  <h2>Templates: iSci Letterhead</h2>
  <p>Remember to download and install fonts before using templates. <a href="/atomic-core/?cat=atoms#typography">Go to Typography.</a></p>
  <!-- List of links -->
	<div tabindex="0" class="onclick-menu">
	    <a href="/assets/templates/documents/isci_letterhead.dotx" download>
	    <div class="circle"><span class="fa fa-download"></span></div>
	    </a>
	</div>
</div>