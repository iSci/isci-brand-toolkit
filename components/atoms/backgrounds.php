<!-- components/atoms/backgrounds.php -->

<h2>Gradients</h2>

<!-- Gradient Purple -> Dark -->
<div class="backgrounds" id="grad-purple-dark">
    <h2><span class="fa fa-paint-brush"></span> #grad-purple-dark</h2>
    <p>.purple (#4b289b) <span class="fa fa-arrow-right"></span> .purple .dark (#24134a)</p>
    
    <!-- List of links -->
<div tabindex="0" class="onclick-menu">
    <a href="/assets/background/gradients/grad-purple-dark.png" download>
    <div class="circle"><span class="fa fa-download"></span></div>
    </a>
</div>

<!-- Copy embed code -->
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-link"></span></div>
    <ul class="onclick-menu-content">
        <li>
            <input type="text" id="grad-purple-dark-link" value="http://brand.interactivescientific.com/assets/background/gradients/grad-purple-dark.png" />
            <button data-copytarget="#grad-purple-dark-link">copy link</button>
        </li>
    </ul>
</div>
</div>

<!-- Gradient Purple -> Light -->
<div class="backgrounds" id="grad-purple-light">
    <h2><span class="fa fa-paint-brush"></span> #grad-purple-light</h2>
    <p>.purple (#4b289b) <span class="fa fa-arrow-right"></span> .purple .light (#7c56d3)</p>
    
    <!-- List of links -->
<div tabindex="0" class="onclick-menu">
    <a href="/assets/background/gradients/grad-purple-light.png" download>
    <div class="circle"><span class="fa fa-download"></span></div>
    </a>
</div>

<!-- Copy embed code -->
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-link"></span></div>
    <ul class="onclick-menu-content">
        <li>
            <input type="text" id="grad-purple-light-link" value="http://brand.interactivescientific.com/assets/background/gradients/grad-purple-light.png" />
            <button data-copytarget="#grad-purple-light-link">copy link</button>
        </li>
    </ul>
</div>
</div>

<!-- Gradient Orange -> Dark -->
<div class="backgrounds" id="grad-orange-dark">
    <h2><span class="fa fa-paint-brush"></span> #grad-orange-dark</h2>
    <p>.orange (#ffa100) <span class="fa fa-arrow-right"></span> .orange .dark (#b37100)</p>
    
    <!-- List of links -->
<div tabindex="0" class="onclick-menu">
    <a href="/assets/background/gradients/grad-orange-dark.png" download>
    <div class="circle"><span class="fa fa-download"></span></div>
    </a>
</div>

<!-- Copy embed code -->
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-link"></span></div>
    <ul class="onclick-menu-content">
        <li>
            <input type="text" id="grad-orange-dark-link" value="http://brand.interactivescientific.com/assets/background/gradients/grad-orange-dark.png" />
            <button data-copytarget="#grad-orange-dark-link">copy link</button>
        </li>
    </ul>
</div>
</div>

<!-- Gradient Orange -> Light -->
<div class="backgrounds" id="grad-orange-light">
    <h2><span class="fa fa-paint-brush"></span> #grad-orange-light</h2>
    <p>.orange (#ffa100) <span class="fa fa-arrow-right"></span> .orange .light (#ffbd4d)</p>
    
    <!-- List of links -->
<div tabindex="0" class="onclick-menu">
    <a href="/assets/background/gradients/grad-orange-light.png" download>
    <div class="circle"><span class="fa fa-download"></span></div>
    </a>
</div>

<!-- Copy embed code -->
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-link"></span></div>
    <ul class="onclick-menu-content">
        <li>
            <input type="text" id="grad-orange-light-link" value="http://brand.interactivescientific.com/assets/background/gradients/grad-orange-light.png" />
            <button data-copytarget="#grad-orange-light-link">copy link</button>
        </li>
    </ul>
</div>
</div>

<!-- Gradient Blue -> Dark -->
<div class="backgrounds" id="grad-blue-dark">
    <h2><span class="fa fa-paint-brush"></span> #grad-blue-dark</h2>
    <p>.blue (#3CB0C7) <span class="fa fa-arrow-right"></span> .blue .dark (#297d8e)</p>
    
    <!-- List of links -->
<div tabindex="0" class="onclick-menu">
    <a href="/assets/background/gradients/grad-blue-dark.png" download>
    <div class="circle"><span class="fa fa-download"></span></div>
    </a>
</div>

<!-- Copy embed code -->
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-link"></span></div>
    <ul class="onclick-menu-content">
        <li>
            <input type="text" id="grad-blue-dark-link" value="http://brand.interactivescientific.com/assets/background/gradients/grad-blue-dark.png" />
            <button data-copytarget="#grad-blue-dark-link">copy link</button>
        </li>
    </ul>
</div>
</div>

<!-- Gradient Blue -> Light -->
<div class="backgrounds" id="grad-blue-light">
    <h2><span class="fa fa-paint-brush"></span> #grad-blue-light</h2>
    <p>.blue (#3CB0C7) <span class="fa fa-arrow-right"></span> .blue .light (#77c8d8)</p>
    
    <!-- List of links -->
<div tabindex="0" class="onclick-menu">
    <a href="/assets/background/gradients/grad-blue-light.png" download>
    <div class="circle"><span class="fa fa-download"></span></div>
    </a>
</div>

<!-- Copy embed code -->
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-link"></span></div>
    <ul class="onclick-menu-content">
        <li>
            <input type="text" id="grad-blue-light-link" value="http://brand.interactivescientific.com/assets/background/gradients/grad-blue-light.png" />
            <button data-copytarget="#grad-blue-light-link">copy link</button>
        </li>
    </ul>
</div>
</div>

<!-- Gradient Green -> Dark -->
<div class="backgrounds" id="grad-green-dark">
    <h2><span class="fa fa-paint-brush"></span> #grad-green-dark</h2>
    <p>.green (#47a425) <span class="fa fa-arrow-right"></span> .green .dark (#2c6617)</p>
    
    <!-- List of links -->
<div tabindex="0" class="onclick-menu">
    <a href="/assets/background/gradients/grad-green-dark.png" download>
    <div class="circle"><span class="fa fa-download"></span></div>
    </a>
</div>

<!-- Copy embed code -->
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-link"></span></div>
    <ul class="onclick-menu-content">
        <li>
            <input type="text" id="grad-green-dark-link" value="http://brand.interactivescientific.com/assets/background/gradients/grad-green-dark.png" />
            <button data-copytarget="#grad-green-dark-link">copy link</button>
        </li>
    </ul>
</div>
</div>

<!-- Gradient Green -> Light -->
<div class="backgrounds" id="grad-green-light">
    <h2><span class="fa fa-paint-brush"></span> #grad-green-light</h2>
    <p>.green (#47a425) <span class="fa fa-arrow-right"></span> .green .light (#69d441)</p>
    
    <!-- List of links -->
<div tabindex="0" class="onclick-menu">
    <a href="/assets/background/gradients/grad-green-light.png" download>
    <div class="circle"><span class="fa fa-download"></span></div>
    </a>
</div>

<!-- Copy embed code -->
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-link"></span></div>
    <ul class="onclick-menu-content">
        <li>
            <input type="text" id="grad-green-light-link" value="http://brand.interactivescientific.com/assets/background/gradients/grad-green-light.png" />
            <button data-copytarget="#grad-green-light-link">copy link</button>
        </li>
    </ul>
</div>
</div>

<h2>Tiles</h2>

<!-- Mosaic -->
<div class="backgrounds" id="tile-mosaic">
    <h2><span class="fa fa-file-image-o"></span> #tile-mosaic</h2>

    <!-- List of links -->
<div tabindex="0" class="onclick-menu">
    <a href="/assets/background/mosaic/mosaic.png" download>
        <div class="circle"><span class="fa fa-download"></span></div>
    </a>
</div>

<!-- Copy embed code -->
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-link"></span></div>
    <ul class="onclick-menu-content">
        <li>
            <input type="text" id="tile-mosaic-link" value="http://brand.interactivescientific.com/assets/background/mosaic/mosaic.png" />
            <button data-copytarget="#tile-mosaic-link">copy link</button>
        </li>
    </ul>
</div>
</div>

<!-- Rainforest -->
<div class="backgrounds" id="tile-rainforest">
    <h2><span class="fa fa-file-image-o"></span> #tile-rainforest</h2>

    <!-- List of links -->
<div tabindex="0" class="onclick-menu">
    <a href="/assets/background/tiles/rainforest.png" download>
        <div class="circle"><span class="fa fa-download"></span></div>
    </a>
    </>
</div>

<!-- Copy embed code -->
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-link"></span></div>
    <ul class="onclick-menu-content">
        <li>
            <input type="text" id="tile-rainforest-link" value="http://brand.interactivescientific.com/assets/background/tiles/rainforest.png" />
            <button data-copytarget="#tile-rainforest-link">copy link</button>
        </li>
    </ul>
</div>
</div>

<!-- Orb -->
<div class="backgrounds" id="tile-orb">
    <h2><span class="fa fa-file-image-o"></span> #tile-orb</h2>

    <!-- List of links -->
<div tabindex="0" class="onclick-menu">
    <a href="/assets/background/tiles/orb.png" download>
        <div class="circle"><span class="fa fa-download"></span></div>
    </a>
</div>

<!-- Copy embed code -->
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-link"></span></div>
    <ul class="onclick-menu-content">
        <li>
            <input type="text" id="tile-orb-link" value="http://brand.interactivescientific.com/assets/background/tiles/orb.png" />
            <button data-copytarget="#tile-orb-link">copy link</button>
        </li>
    </ul>
</div>
</div>