<!-- components/readme/brand.php -->

<div class="brand ambition" id="grad-blue-dark">
    <p>Ambition</p>
    <h1>To create a distinctive, emotive, and future facing brand proposition</h1>
</div>

<div class="brand purpose" id="grad-purple-dark">
    <p>Purpose</p>
    <h1>To excite future generations and make scientific exploration accessible to all</h1>
</div>

<div class="brand essence" id="grad-orange-dark">
    <p>Essence</p>
    <h1>Excitement for all</h1>
</div>