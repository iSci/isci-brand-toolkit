<!-- components/atoms/logos.php -->

<!-- iSci Master logo -->
<h2>Master Logo</h2>
<div class="logos">
    <div class="panel">
        <img src="assets/logos/iSci-png/iSci_black.png" height="200px" />
        <div tabindex="0" class="onclick-menu">
            <div class="circle"><span class="fa fa-link"></span></div>
            <ul class="onclick-menu-content">
                <li>
                    <!-- Black -->
                    <label for="black">Black:</label>
                    <input type="text" id="black" value="http://brand.interactivescientific.com/assets/logos/iSci-png/iSci_black.png" />
                    <button data-copytarget="#black">copy link</button>
                </li>
                <li>
                    <!-- White -->
                    <label for="white">White:</label>
                    <input type="text" id="white" value="http://brand.interactivescientific.com/assets/logos/iSci-png/iSci_white.png" />
                    <button data-copytarget="#white">copy link</button>
                </li>
                <li>
                    <!-- Purple -->
                    <label for="purple">Purple:</label>
                    <input type="text" id="purple" value="http://brand.interactivescientific.com/assets/logos/iSci-png/iSci_purple.png" />
                    <button data-copytarget="#purple">copy link</button>
                </li>
                <li>
                    <!-- Orange -->
                    <label for="orange">Orange:</label>
                    <input type="text" id="orange" value="http://brand.interactivescientific.com/assets/logos/iSci-png/iSci_orange.png" />
                    <button data-copytarget="#orange">copy link</button>
                </li>
                <li>
                    <!-- Blue -->
                    <label for="blue">Blue:</label>
                    <input type="text" id="blue" value="http://brand.interactivescientific.com/assets/logos/iSci-png/iSci_blue.png" />
                    <button data-copytarget="#blue">copy link</button>
                </li>
                <li>
                    <!-- Green -->
                    <label for="green">Green:</label>
                    <input type="text" id="green" value="http://brand.interactivescientific.com/assets/logos/iSci-png/iSci_green.png" />
                    <button data-copytarget="#green">copy link</button>
                </li>
            </ul>
        </div>
        <div tabindex="0" class="onclick-menu">
            <div class="circle"><span class="fa fa-download"></span></div>
            <ul class="onclick-menu-content">
                <li><a href="assets/logos/iSci-png.zip" download>All Colours (PNG)</a></li>
                <li><a href="assets/svg-outline/logo.svg" download>Vector (SVG)</a></li>
                <li><a href="assets/logos/iSci.ai" download>Design (AI)</a></li>
            </ul>
        </div>
    </div>
</div>

<!-- iSci Small logo -->
<h2>Small Logo</h2>
<div class="logos">
    <div class="panel">
        <img src="assets/logos/iSci_small-png/iSci_small_black.png" height="200px" />
        <br>
        <div tabindex="0" class="onclick-menu">
            <div class="circle"><span class="fa fa-link"></span></div>
            <ul class="onclick-menu-content">
                <li>
                    <!-- Black -->
                    <label for="small_black">Black:</label>
                    <input type="text" id="small_black" value="http://brand.interactivescientific.com/assets/logos/iSci_small-png/iSci_small_black.png" />
                    <button data-copytarget="#small_black">copy link</button>
                </li>
                <li>
                    <!-- White -->
                    <label for="small_white">White:</label>
                    <input type="text" id="small_white" value="http://brand.interactivescientific.com/assets/logos/iSci_small-png/iSci_small_white.png" />
                    <button data-copytarget="#small_white">copy link</button>
                </li>
                <li>
                    <!-- Purple -->
                    <label for="small_purple">Purple:</label>
                    <input type="text" id="small_purple" value="http://brand.interactivescientific.com/assets/logos/iSci_small-png/iSci_small_purple.png" />
                    <button data-copytarget="#small_purple">copy link</button>
                </li>
                <li>
                    <!-- Orange -->
                    <label for="small_orange">Orange:</label>
                    <input type="text" id="small_orange" value="http://brand.interactivescientific.com/assets/logos/iSci_small-png/iSci_small_orange.png" />
                    <button data-copytarget="#small_orange">copy link</button>
                </li>
                <li>
                    <!-- Blue -->
                    <label for="small_blue">Blue:</label>
                    <input type="text" id="small_blue" value="http://brand.interactivescientific.com/assets/logos/iSci_small-png/iSci_small_blue.png" />
                    <button data-copytarget="#small_blue">copy link</button>
                </li>
                <li>
                    <!-- Green -->
                    <label for="small_green">Green:</label>
                    <input type="text" id="small_green" value="http://brand.interactivescientific.com/assets/logos/iSci_small-png/iSci_small_green.png" />
                    <button data-copytarget="#small_green">copy link</button>
                </li>
            </ul>
        </div>
        <div tabindex="0" class="onclick-menu">
            <div class="circle"><span class="fa fa-download"></span></div>
            <ul class="onclick-menu-content">
                <li><a href="assets/logos/iSci_small-png.zip" download>All Colours (PNG)</a></li>
                <li><a href="assets/svg-outline/logo-small.svg" download>Vector (SVG)</a></li>
                <li><a href="assets/logos/iSci_small.ai" download>Design (AI)</a></li>
            </ul>
        </div>
    </div>
</div>

<!-- NSB Banner logo -->
<h2>Nano Simbox Banner</h2>
<div class="logos">
    <div class="panel">
        <img src="assets/logos/nsb_banner-png/nsb_banner_black.png" width="300px" />
        <div tabindex="0" class="onclick-menu">
            <div class="circle"><span class="fa fa-link"></span></div>
            <ul class="onclick-menu-content">
                <li>
                    <!-- Black -->
                    <label for="nsb_black">Black:</label>
                    <input type="text" id="nsb_black" value="http://brand.interactivescientific.com/assets/logos/nsb_banner-png/nsb_banner_black.png" />
                    <button data-copytarget="#nsb_black">copy link</button>
                </li>
                <li>
                    <!-- White -->
                    <label for="nsb_white">White:</label>
                    <input type="text" id="nsb_white" value="http://brand.interactivescientific.com/assets/logos/nsb_banner-png/nsb_banner_white.png" />
                    <button data-copytarget="#nsb_white">copy link</button>
                </li>
                <li>
                    <!-- Purple -->
                    <label for="nsb_purple">Purple:</label>
                    <input type="text" id="nsb_purple" value="http://brand.interactivescientific.com/assets/logos/nsb_banner-png/nsb_banner_purple.png" />
                    <button data-copytarget="#nsb_purple">copy link</button>
                </li>
                <li>
                    <!-- Orange -->
                    <label for="nsb_orange">Orange:</label>
                    <input type="text" id="nsb_orange" value="http://brand.interactivescientific.com/assets/logos/nsb_banner-png/nsb_banner_orange.png" />
                    <button data-copytarget="#nsb_orange">copy link</button>
                </li>
                <li>
                    <!-- Blue -->
                    <label for="nsb_blue">Blue:</label>
                    <input type="text" id="nsb_blue" value="http://brand.interactivescientific.com/assets/logos/nsb_banner-png/nsb_banner_blue.png" />
                    <button data-copytarget="#nsb_blue">copy link</button>
                </li>
                <li>
                    <!-- Green -->
                    <label for="nsb_green">Green:</label>
                    <input type="text" id="nsb_green" value="http://brand.interactivescientific.com/assets/logos/nsb_banner-png/nsb_banner_green.png" />
                    <button data-copytarget="#nsb_green">copy link</button>
                </li>
            </ul>
        </div>
        <div tabindex="0" class="onclick-menu">
            <div class="circle"><span class="fa fa-download"></span></div>
            <ul class="onclick-menu-content">
                <li><a href="assets/logos/nsb_banner-png.zip" download>All Colours (PNG)</a></li>
                <li><a href="assets/svg-outline/nsb_banner.svg" download>Vector (SVG)</a></li>
                <li><a href="assets/logos/nsb_banner.ai" download>Design (AI)</a></li>
            </ul>
        </div>
    </div>
</div>

<!-- NSB Icon logo -->
<h2>Nano Simbox Icon</h2>
<div class="logos">
    <div class="panel">
        <img src="assets/logos/nsb_icon-png/nsb_icon_black.png" height="200px" /><br>
        <div tabindex="0" class="onclick-menu">
            <div class="circle"><span class="fa fa-link"></span></div>
            <ul class="onclick-menu-content">
                <li>
                    <!-- Black -->
                    <label for="nsb_icon_black">Black:</label>
                    <input type="text" id="nsb_icon_black" value="http://brand.interactivescientific.com/assets/logos/nsb_icon-png/nsb_icon_black.png" />
                    <button data-copytarget="#nsb_icon_black">copy link</button>
                </li>
                <li>
                    <!-- White -->
                    <label for="nsb_icon_white">White:</label>
                    <input type="text" id="nsb_icon_white" value="http://brand.interactivescientific.com/assets/logos/nsb_icon-png/nsb_icon_white.png" />
                    <button data-copytarget="#nsb_icon_white">copy link</button>
                </li>
            </ul>
        </div>
        <div tabindex="0" class="onclick-menu">
            <div class="circle"><span class="fa fa-download"></span></div>
            <ul class="onclick-menu-content">
                <li><a href="assets/logos/nsb_icon-png.zip" download>Black & White (PNG)</a></li>
                <li><a href="assets/svg-outline/nsb_icon.svg" download>Vector (SVG)</a></li>
                <li><a href="assets/logos/nsb_icon.ai" download>Design (AI)</a></li>
            </ul>
        </div>
    </div>
</div>

<!-- NSB Text logo -->
<h2>Nano Simbox Logotype</h2>
<div class="logos">
    <div class="panel">
        <img src="assets/logos/nsb_text-png/nsb_text_black.png" width="300px" /><br>
        <div tabindex="0" class="onclick-menu">
            <div class="circle"><span class="fa fa-link"></span></div>
            <ul class="onclick-menu-content">
                <li>
                    <!-- Black -->
                    <label for="nsb_text_black">Black:</label>
                    <input type="text" id="nsb_text_black" value="http://brand.interactivescientific.com/assets/logos/nsb_text-png/nsb_text_black.png" />
                    <button data-copytarget="#nsb_text_black">copy link</button>
                </li>
                <li>
                    <!-- White -->
                    <label for="nsb_text_white">White:</label>
                    <input type="text" id="nsb_text_white" value="http://brand.interactivescientific.com/assets/logos/nsb_text-png/nsb_text_white.png" />
                    <button data-copytarget="#nsb_text_white">copy link</button>
                </li>
            </ul>
        </div>
        <div tabindex="0" class="onclick-menu">
            <div class="circle"><span class="fa fa-download"></span></div>
            <ul class="onclick-menu-content">
                <li><a href="assets/logos/nsb_text-png.zip" download>Black & White (PNG)</a></li>
                <li><a href="assets/svg-outline/nsb_text.svg" download>Vector (SVG)</a></li>
                <li><a href="assets/logos/nsb_text.ai" download>Design (AI)</a></li>
            </ul>
        </div>
    </div>
</div>