<!DOCTYPE html>
<html>
<head>
    <title>iSci Brand Toolkit</title>

    <?php include("atomic-head.php"); ?>

    <link rel="stylesheet" type="text/css" href="atomic-core/css/main.css">
    <link rel="stylesheet" type="text/css" href="atomic-core/css/demo.css">

    <link rel="stylesheet" href="atomic-core/font-awesome/css/font-awesome.min.css">

    <link rel="apple-touch-icon" sizes="180x180" href="/atomic-core/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/atomic-core/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/atomic-core/favicon/favicon-16x16.png">
    <link rel="manifest" href="/atomic-core/favicon/site.webmanifest">
    <link rel="mask-icon" href="/atomic-core/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/atomic-core/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="msapplication-config" content="/atomic-core/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

</head>

<body>



<div class="indexDemo">

    <div id="intro">
        <div class="full" id="grad-purple-dark">
            <div class="stamp animate-open">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 789.3 643.5" style="enable-background:new 0 0 789.3 643.5;" xml:space="preserve">
                <style type="text/css">
                    .st0{fill:none;stroke:#000000;stroke-width:21.2598;}
                    .si1{font-family:'galano_grotesque_demobold';}
                    .si2{font-size:279.8002px;}
                </style>
                <path class="st0" d="M216.2,433.5v84.1L10.8,626.1v-609l205.4,108.4v70"/>
                <text transform="matrix(1 0 0 1 42.4627 406.7269)" class="si1 si2">iSci</text>
                </svg>
            </div>

            <div>
                <h1 class="wide">Creating a distinctive, emotive, and future facing brand</h1>
                <div class="third">
                    <h2>In Autumn we started developing the Interactive Scientific brand strategy. </h2>

                    <p>Today we are launching our Visual Brand Toolkit. This is a living, breathing style guide which includes the initial building blocks that make the Interactive Scientific brand. <p>

                    <p class="space">We expect changes as we continue to develop and grow our exciting brand story.</p>
                    <a href="/atomic-core/?cat=readme" class="button">Go to the toolkit</a>
                </div>
            </div>
            <a href="/#brand">
                <div class="continue">
                    <span class="fa fa-arrow-circle-o-down"></span>
                </div>
            </a>
        </div>
    </div>

    <div id="brand">
        <div class="full purple dark">
            <p>Our brand purpose</p>
            <h1>To excite future generations and make scientific exploration accessible to all</h1>
            <p>Our brand essence</p>
            <h1>Excitement for all</h1>
             <a href="/#manifesto">
                <div class="continue">
                    <span class="fa fa-arrow-circle-o-down"></span>
                </div>
            </a>
        </div>
    </div>

     <div id="manifesto">
        <div class="full black alt">
        <div class="manifesto black alt">
            <h1>Refine</h1>
            <p>not Rebrand</p>
        </div>
        <div class="manifesto black light">
            <h1>A toolkit</h1>
            <p>not a logo</p>
        </div>
        <div class="manifesto black alt">
            <h1>Open design</h1>
            <p>approach</p>
        </div>
        <div class="manifesto black light">
            <h1>Align</h1>
            <p>with our brand purpose</p>
        </div>
        <div class="manifesto black alt">
            <h1>Objectivity</h1>
            <p>not subjectivity</p>
        </div>
        <div class="manifesto black light">
            <h1>Experience</h1>
            <p>not identity</p>
        </div>
        <div class="manifesto black alt">
            <h1>Changing</h1>
            <p>not controlling</p>
        </div>
        <div class="manifesto black light">
            <h1>Flexible</h1>
            <p>and grow with us</p>
        </div>
        <div class="manifesto black alt">
            <h1>Always question</h1>
            <p>this brief</p>
        </div>
        <a href="/#story">
                <div class="continue">
                    <span class="fa fa-arrow-circle-o-down"></span>
                </div>
            </a>
        </div>
    </div>

    <div id="story">
        <div class="full" id="grad-blue-dark">
            <svg version="1.1" class="story" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 789.3 643.5" style="enable-background:new 0 0 789.3 643.5;" xml:space="preserve">
            <style type="text/css">
                .st0{fill:none;stroke:#000000;stroke-width:21.2598;}
                .st1{font-family:'galano_grotesque_demobold';}
                .st2{font-size:131.2294px;}
                .st3{font-size:131.23px;}
            </style>
            <path class="st0" d="M216.2,433.5v84.1L10.8,626.1v-609l205.4,108.4v70"/>
            <text transform="matrix(1 0 0 1 73.9134 301.5081)" class="st1 st2">interactive</text>
            <text transform="matrix(0.9976 0 0 1 36.2593 413.4473)" class="st1 st3">Scientific</text>
            </svg>
            <p>Our visual brand story</p>
            <h1>Everyone is welcome to come in: <span class="blue light"> transparent & accessible. </span> </h1>
            <h1><span class="blue light">Open the door</span> to everyday global challenges that affect us all. </h1>
            <h1>What do molecules look like? </h1>
            <h1>Abstract science concepts lead to learning experiences. </h1>
            <h1>A window frame for molecular aesthetics. </h1>
            <h1>Interactive Scientific: <span class="blue light">The Door to the Nano World</span></h1>
             <a href="/#type">
                <div class="continue">
                    <span class="fa fa-arrow-circle-o-down"></span>
                </div>
            </a>
        </div>
    </div>

   <div id="type">
    <div class="full" id="grad-orange-dark">
        <h1>Typography</h1>
        <h1 class="typography logofont">Galano Grotesque</h1>
        <h2 class="logofont">We use Galano Grotesque by Rene Bieder as our primary logo typeface.</h2>
        <h1 class="typography font1">Source Sans Pro</h1>
        <h2 class="font1">Source Sans is our go to font for just about everything else including body text, user interface, company documents etc</h2>
        <p class="space"></p>
        <a href="/atomic-core/?cat=atoms#typography" class="button">Go to typography</a>
       
       <a href="/#colours">
                <div class="continue">
                    <span class="fa fa-arrow-circle-o-down"></span>
                </div>
            </a>
    </div>
    </div>

    <div id="colours">
    <div class="full white alt">

        <div class="manifesto purple">
          <svg version="1.1" class="logo-white" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 789.3 643.5" style="enable-background:new 0 0 789.3 643.5;" xml:space="preserve">
            <style type="text/css">
                .st0{fill:none;stroke:#000000;stroke-width:21.2598;}
                .st1{font-family:'galano_grotesque_demobold';}
                .st2{font-size:131.2294px;}
                .st3{font-size:131.23px;}
            </style>
            <path class="st0" d="M216.2,433.5v84.1L10.8,626.1v-609l205.4,108.4v70"/>
            <text transform="matrix(1 0 0 1 73.9134 301.5081)" class="st1 st2">interactive</text>
            <text transform="matrix(0.9976 0 0 1 36.2593 413.4473)" class="st1 st3">Scientific</text>
            </svg>
        </div>
        <div class="manifesto orange">
          <svg version="1.1" class="logo-white" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 789.3 643.5" style="enable-background:new 0 0 789.3 643.5;" xml:space="preserve">
            <style type="text/css">
                .st0{fill:none;stroke:#000000;stroke-width:21.2598;}
                .st1{font-family:'galano_grotesque_demobold';}
                .st2{font-size:131.2294px;}
                .st3{font-size:131.23px;}
            </style>
            <path class="st0" d="M216.2,433.5v84.1L10.8,626.1v-609l205.4,108.4v70"/>
            <text transform="matrix(1 0 0 1 73.9134 301.5081)" class="st1 st2">interactive</text>
            <text transform="matrix(0.9976 0 0 1 36.2593 413.4473)" class="st1 st3">Scientific</text>
            </svg>
        </div>
        <div class="manifesto blue">
          <svg version="1.1" class="logo-white" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 789.3 643.5" style="enable-background:new 0 0 789.3 643.5;" xml:space="preserve">
            <style type="text/css">
                .st0{fill:none;stroke:#000000;stroke-width:21.2598;}
                .st1{font-family:'galano_grotesque_demobold';}
                .st2{font-size:131.2294px;}
                .st3{font-size:131.23px;}
            </style>
            <path class="st0" d="M216.2,433.5v84.1L10.8,626.1v-609l205.4,108.4v70"/>
            <text transform="matrix(1 0 0 1 73.9134 301.5081)" class="st1 st2">interactive</text>
            <text transform="matrix(0.9976 0 0 1 36.2593 413.4473)" class="st1 st3">Scientific</text>
            </svg>
        </div>

        <div class="manifesto purple light">
          <svg version="1.1" class="logo-white" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 789.3 643.5" style="enable-background:new 0 0 789.3 643.5;" xml:space="preserve">
            <style type="text/css">
                .st0{fill:none;stroke:#000000;stroke-width:21.2598;}
                .st1{font-family:'galano_grotesque_demobold';}
                .st2{font-size:131.2294px;}
                .st3{font-size:131.23px;}
            </style>
            <path class="st0" d="M216.2,433.5v84.1L10.8,626.1v-609l205.4,108.4v70"/>
            <text transform="matrix(1 0 0 1 73.9134 301.5081)" class="st1 st2">interactive</text>
            <text transform="matrix(0.9976 0 0 1 36.2593 413.4473)" class="st1 st3">Scientific</text>
            </svg>
        </div>
        <div class="manifesto orange light">
          <svg version="1.1" class="logo-white" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 789.3 643.5" style="enable-background:new 0 0 789.3 643.5;" xml:space="preserve">
            <style type="text/css">
                .st0{fill:none;stroke:#000000;stroke-width:21.2598;}
                .st1{font-family:'galano_grotesque_demobold';}
                .st2{font-size:131.2294px;}
                .st3{font-size:131.23px;}
            </style>
            <path class="st0" d="M216.2,433.5v84.1L10.8,626.1v-609l205.4,108.4v70"/>
            <text transform="matrix(1 0 0 1 73.9134 301.5081)" class="st1 st2">interactive</text>
            <text transform="matrix(0.9976 0 0 1 36.2593 413.4473)" class="st1 st3">Scientific</text>
            </svg>
        </div>
        <div class="manifesto blue light">
          <svg version="1.1" class="logo-white" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 789.3 643.5" style="enable-background:new 0 0 789.3 643.5;" xml:space="preserve">
            <style type="text/css">
                .st0{fill:none;stroke:#000000;stroke-width:21.2598;}
                .st1{font-family:'galano_grotesque_demobold';}
                .st2{font-size:131.2294px;}
                .st3{font-size:131.23px;}
            </style>
            <path class="st0" d="M216.2,433.5v84.1L10.8,626.1v-609l205.4,108.4v70"/>
            <text transform="matrix(1 0 0 1 73.9134 301.5081)" class="st1 st2">interactive</text>
            <text transform="matrix(0.9976 0 0 1 36.2593 413.4473)" class="st1 st3">Scientific</text>
            </svg>
        </div>

        <div class="manifesto purple dark">
          <svg version="1.1" class="logo-white" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 789.3 643.5" style="enable-background:new 0 0 789.3 643.5;" xml:space="preserve">
            <style type="text/css">
                .st0{fill:none;stroke:#000000;stroke-width:21.2598;}
                .st1{font-family:'galano_grotesque_demobold';}
                .st2{font-size:131.2294px;}
                .st3{font-size:131.23px;}
            </style>
            <path class="st0" d="M216.2,433.5v84.1L10.8,626.1v-609l205.4,108.4v70"/>
            <text transform="matrix(1 0 0 1 73.9134 301.5081)" class="st1 st2">interactive</text>
            <text transform="matrix(0.9976 0 0 1 36.2593 413.4473)" class="st1 st3">Scientific</text>
            </svg>
        </div>
        <div class="manifesto orange dark">
          <svg version="1.1" class="logo-white" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 789.3 643.5" style="enable-background:new 0 0 789.3 643.5;" xml:space="preserve">
            <style type="text/css">
                .st0{fill:none;stroke:#000000;stroke-width:21.2598;}
                .st1{font-family:'galano_grotesque_demobold';}
                .st2{font-size:131.2294px;}
                .st3{font-size:131.23px;}
            </style>
            <path class="st0" d="M216.2,433.5v84.1L10.8,626.1v-609l205.4,108.4v70"/>
            <text transform="matrix(1 0 0 1 73.9134 301.5081)" class="st1 st2">interactive</text>
            <text transform="matrix(0.9976 0 0 1 36.2593 413.4473)" class="st1 st3">Scientific</text>
            </svg>
        </div>
        <div class="manifesto blue dark">
          <svg version="1.1" class="logo-white" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 789.3 643.5" style="enable-background:new 0 0 789.3 643.5;" xml:space="preserve">
            <style type="text/css">
                .st0{fill:none;stroke:#000000;stroke-width:21.2598;}
                .st1{font-family:'galano_grotesque_demobold';}
                .st2{font-size:131.2294px;}
                .st3{font-size:131.23px;}
            </style>
            <path class="st0" d="M216.2,433.5v84.1L10.8,626.1v-609l205.4,108.4v70"/>
            <text transform="matrix(1 0 0 1 73.9134 301.5081)" class="st1 st2">interactive</text>
            <text transform="matrix(0.9976 0 0 1 36.2593 413.4473)" class="st1 st3">Scientific</text>
            </svg>
        </div>

       
       <a href="/#door">
                <div class="continue">
                    <span class="fa fa-arrow-circle-o-down"></span>
                </div>
            </a>
    </div>
    </div>

    <div id="door">
    <div class="full white dark">

        <div class="door-panel white dark">

        <div class="logo animate-open">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 789.3 643.5" style="enable-background:new 0 0 789.3 643.5;" xml:space="preserve">
                <style type="text/css">
                    .st0{fill:none;stroke:#000000;stroke-width:21.2598;}
                    .st1{font-family:'galano_grotesque_demobold';}
                    .st2{font-size:131.2294px;}
                    .st3{font-size:131.23px;}
                </style>
                <path class="st0" d="M216.2,433.5v84.1L10.8,626.1v-609l205.4,108.4v70"/>
                <g>
                    <polygon points="0.2,643.7 0.2,-0.5 226.8,119.1 226.8,195.5 205.5,195.5 205.5,131.9 21.4,34.7 21.4,608.5 205.5,511.2 
                        205.5,433.5 226.8,433.5 226.8,524.1     "/>
                </g>
                <text transform="matrix(1 0 0 1 73.9134 301.5081)" class="st1 st2">interactive</text>
                <text transform="matrix(0.9976 0 0 1 36.2591 413.4471)" class="st1 st3">Scientific</text>
                </svg>
            </div>
        </div>


        <!-- Door example with rainforest -->
<div class="door-panel blue dark">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
    <filter id="inset-shadow" x="-50%" y="-50%" width="200%" height="200%">
    <feComponentTransfer in=SourceAlpha>
      <feFuncA type="table" tableValues="1 0" />
    </feComponentTransfer>
    <feGaussianBlur stdDeviation="8"/>
    <feOffset dx="-8" dy="-8" result="offsetblur"/>
    <feFlood flood-color="rgb(20, 0, 0)" result="color"/>
    <feComposite in2="offsetblur" operator="in"/>
    <feComposite in2="SourceAlpha" operator="in" />
    <feMerge>
      <feMergeNode in="SourceGraphic" />
      <feMergeNode />
    </feMerge>
  </filter> 
  <defs>
  <pattern id="forest" patternUnits="userSpaceOnUse" width="2048px" height="1535px">
    <image xlink:href="/assets/background/tiles/rainforest.png" x="-500" y="0" width="1200px" height="899px" />
  </pattern>
</defs>
<path class="door" fill="url(#forest)" filter="url(#inset-shadow)" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg> 
<div class="message"><h1>Find out more about glucose</h1>
</div>
<div class="foreground"><img class="rotate" src="/assets/foreground/glucose.png">
</div>
</div>

        <!-- Door example with orb -->
<div class="door-panel purple dark">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
    <filter id="inset-shadow" x="-50%" y="-50%" width="200%" height="200%">
    <feComponentTransfer in=SourceAlpha>
      <feFuncA type="table" tableValues="1 0" />
    </feComponentTransfer>
    <feGaussianBlur stdDeviation="8"/>
    <feOffset dx="-8" dy="-8" result="offsetblur"/>
    <feFlood flood-color="rgb(20, 0, 0)" result="color"/>
    <feComposite in2="offsetblur" operator="in"/>
    <feComposite in2="SourceAlpha" operator="in" />
    <feMerge>
      <feMergeNode in="SourceGraphic" />
      <feMergeNode />
    </feMerge>
  </filter> 
  <defs>
  <pattern id="orb" patternUnits="userSpaceOnUse" width="2048px" height="1535px">
    <image xlink:href="/assets/background/tiles/orb.png" x="-750" y="0" width="1200px" height="899px" />
  </pattern>
</defs>
<path class="door" fill="url(#orb)" filter="url(#inset-shadow)" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg>
<div class="message"><h1>Explore <br>Nano <br>Simbox</h1>
</div>
<div class="foreground"><img class="grow" src="/assets/foreground/glucose.png">
</div>    
</div>

     
    </div>
    </div>


</div>
</body>

<?php include("atomic-foot.php"); ?>

</html>



