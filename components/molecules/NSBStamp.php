<!-- components/molecules/NSBStamp.php -->

<!-- NSB stamp - purple dark -->
<div class="griditem">
	<h4>.nsbstamp #grad-purple-dark</h4>
	<div class="nsbstamp" id="grad-purple-dark">
	<img src="assets/logos/nsb_icon-png/nsb_icon_white.png" height="200px" />
	</div>

	<!-- List of links -->
	<div tabindex="0" class="onclick-menu">
	    <a href="/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_purplegrad_dark.png" download>
	    <div class="circle"><span class="fa fa-download"></span></div>
	    </a>
	</div>

	<!-- Copy embed code -->
	<div tabindex="0" class="onclick-menu">
	    <div class="circle"><span class="fa fa-link"></span></div>
	    <ul class="onclick-menu-content">
	        <li>
	            <input type="text" id="nsbstamp-grad-purple-dark-link" value="http://brand.interactivescientific.com/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_purplegrad_dark.png" />
	            <button data-copytarget="#nsbstamp-grad-purple-dark-link">copy link</button>
	        </li>
	    </ul>
	</div>
</div>

<!-- NSB stamp - purple light -->
<div class="griditem">
	<h4>.nsbstamp #grad-purple-light</h4>
	<div class="nsbstamp" id="grad-purple-light">
	<img src="assets/logos/nsb_icon-png/nsb_icon_white.png" height="200px" />
	</div>

	<!-- List of links -->
	<div tabindex="0" class="onclick-menu">
	    <a href="/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_purplegrad_light.png" download>
	    <div class="circle"><span class="fa fa-download"></span></div>
	    </a>
	</div>

	<!-- Copy embed code -->
	<div tabindex="0" class="onclick-menu">
	    <div class="circle"><span class="fa fa-link"></span></div>
	    <ul class="onclick-menu-content">
	        <li>
	            <input type="text" id="nsbstamp-grad-purple-light-link" value="http://brand.interactivescientific.com/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_purplegrad_light.png" />
	            <button data-copytarget="#nsbstamp-grad-purple-light-link">copy link</button>
	        </li>
	    </ul>
	</div>
</div>

<!-- NSB stamp - orange dark -->
<div class="griditem">
	<h4>.nsbstamp #grad-orange-dark</h4>
	<div class="nsbstamp" id="grad-orange-dark">
	<img src="assets/logos/nsb_icon-png/nsb_icon_white.png" height="200px" />
	</div>

	<!-- List of links -->
	<div tabindex="0" class="onclick-menu">
	    <a href="/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_orangegrad_dark.png" download>
	    <div class="circle"><span class="fa fa-download"></span></div>
	    </a>
	</div>

	<!-- Copy embed code -->
	<div tabindex="0" class="onclick-menu">
	    <div class="circle"><span class="fa fa-link"></span></div>
	    <ul class="onclick-menu-content">
	        <li>
	            <input type="text" id="nsbstamp-grad-orange-dark-link" value="http://brand.interactivescientific.com/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_orangegrad_dark.png" />
	            <button data-copytarget="#nsbstamp-grad-orange-dark-link">copy link</button>
	        </li>
	    </ul>
	</div>
</div>

<!-- NSB stamp - orange light -->
<div class="griditem">
	<h4>.nsbstamp #grad-orange-light</h4>
	<div class="nsbstamp" id="grad-orange-light">
	<img src="assets/logos/nsb_icon-png/nsb_icon_white.png" height="200px" />
	</div>

	<!-- List of links -->
	<div tabindex="0" class="onclick-menu">
	    <a href="/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_orangegrad_light.png" download>
	    <div class="circle"><span class="fa fa-download"></span></div>
	    </a>
	</div>

	<!-- Copy embed code -->
	<div tabindex="0" class="onclick-menu">
	    <div class="circle"><span class="fa fa-link"></span></div>
	    <ul class="onclick-menu-content">
	        <li>
	            <input type="text" id="nsbstamp-grad-orange-light-link" value="http://brand.interactivescientific.com/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_orangegrad_light.png" />
	            <button data-copytarget="#nsbstamp-grad-orange-light-link">copy link</button>
	        </li>
	    </ul>
	</div>
</div>

<!-- NSB stamp - blue dark -->
<div class="griditem">
	<h4>.nsbstamp #grad-blue-dark</h4>
	<div class="nsbstamp" id="grad-blue-dark">
	<img src="assets/logos/nsb_icon-png/nsb_icon_white.png" height="200px" />
	</div>

	<!-- List of links -->
	<div tabindex="0" class="onclick-menu">
	    <a href="/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_bluegrad_dark.png" download>
	    <div class="circle"><span class="fa fa-download"></span></div>
	    </a>
	</div>

	<!-- Copy embed code -->
	<div tabindex="0" class="onclick-menu">
	    <div class="circle"><span class="fa fa-link"></span></div>
	    <ul class="onclick-menu-content">
	        <li>
	            <input type="text" id="nsbstamp-grad-blue-dark-link" value="http://brand.interactivescientific.com/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_bluegrad_dark.png" />
	            <button data-copytarget="#nsbstamp-grad-blue-dark-link">copy link</button>
	        </li>
	    </ul>
	</div>
</div>

<!-- NSB stamp - blue light -->
<div class="griditem">
	<h4>.nsbstamp #grad-blue-light</h4>
	<div class="nsbstamp" id="grad-blue-light">
	<img src="assets/logos/nsb_icon-png/nsb_icon_white.png" height="200px" />
	</div>

	<!-- List of links -->
	<div tabindex="0" class="onclick-menu">
	    <a href="/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_bluegrad_light.png" download>
	    <div class="circle"><span class="fa fa-download"></span></div>
	    </a>
	</div>

	<!-- Copy embed code -->
	<div tabindex="0" class="onclick-menu">
	    <div class="circle"><span class="fa fa-link"></span></div>
	    <ul class="onclick-menu-content">
	        <li>
	            <input type="text" id="nsbstamp-grad-blue-light-link" value="http://brand.interactivescientific.com/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_bluegrad_light.png" />
	            <button data-copytarget="#nsbstamp-grad-blue-light-link">copy link</button>
	        </li>
	    </ul>
	</div>
</div>

<!-- NSB stamp - green dark -->
<div class="griditem">
	<h4>.nsbstamp #grad-green-dark</h4>
	<div class="nsbstamp" id="grad-green-dark">
	<img src="assets/logos/nsb_icon-png/nsb_icon_white.png" height="200px" />
	</div>

	<!-- List of links -->
	<div tabindex="0" class="onclick-menu">
	    <a href="/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_greengrad_dark.png" download>
	    <div class="circle"><span class="fa fa-download"></span></div>
	    </a>
	</div>

	<!-- Copy embed code -->
	<div tabindex="0" class="onclick-menu">
	    <div class="circle"><span class="fa fa-link"></span></div>
	    <ul class="onclick-menu-content">
	        <li>
	            <input type="text" id="nsbstamp-grad-green-dark-link" value="http://brand.interactivescientific.com/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_greengrad_dark.png" />
	            <button data-copytarget="#nsbstamp-grad-green-dark-link">copy link</button>
	        </li>
	    </ul>
	</div>
</div>

<!-- NSB stamp - green light -->
<div class="griditem">
	<h4>.nsbstamp #grad-green-light</h4>
	<div class="nsbstamp" id="grad-green-light">
	<img src="assets/logos/nsb_icon-png/nsb_icon_white.png" height="200px" />
	</div>

	<!-- List of links -->
	<div tabindex="0" class="onclick-menu">
	    <a href="/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_greengrad_light.png" download>
	    <div class="circle"><span class="fa fa-download"></span></div>
	    </a>
	</div>

	<!-- Copy embed code -->
	<div tabindex="0" class="onclick-menu">
	    <div class="circle"><span class="fa fa-link"></span></div>
	    <ul class="onclick-menu-content">
	        <li>
	            <input type="text" id="nsbstamp-grad-green-light-link" value="http://brand.interactivescientific.com/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_greengrad_light.png" />
	            <button data-copytarget="#nsbstamp-grad-green-light-link">copy link</button>
	        </li>
	    </ul>
	</div>
</div>

<!-- NSB stamp - rainforest -->
<div class="griditem">
	<h4>.nsbstamp #tile-rainforest</h4>
	<div class="nsbstamp" id="tile-rainforest">
	<img src="assets/logos/nsb_icon-png/nsb_icon_white.png" height="200px" />
	</div>

	<!-- List of links -->
	<div tabindex="0" class="onclick-menu">
	    <a href="/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_rainforest.png" download>
	    <div class="circle"><span class="fa fa-download"></span></div>
	    </a>
	</div>

	<!-- Copy embed code -->
	<div tabindex="0" class="onclick-menu">
	    <div class="circle"><span class="fa fa-link"></span></div>
	    <ul class="onclick-menu-content">
	        <li>
	            <input type="text" id="nsbstamp_rainforest-link" value="http://brand.interactivescientific.com/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_rainforest.png" />
	            <button data-copytarget="#nsbstamp_rainforest-link">copy link</button>
	        </li>
	    </ul>
	</div>
</div>

<!-- NSB stamp - orb -->
<div class="griditem">
	<h4>.nsbstamp #tile-orb</h4>
	<div class="nsbstamp" id="tile-orb">
	<img src="assets/logos/nsb_icon-png/nsb_icon_white.png" height="200px" />
	</div>

	<!-- List of links -->
	<div tabindex="0" class="onclick-menu">
	    <a href="/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_orb.png" download>
	    <div class="circle"><span class="fa fa-download"></span></div>
	    </a>
	</div>

	<!-- Copy embed code -->
	<div tabindex="0" class="onclick-menu">
	    <div class="circle"><span class="fa fa-link"></span></div>
	    <ul class="onclick-menu-content">
	        <li>
	            <input type="text" id="nsbstamp_orb-link" value="http://brand.interactivescientific.com/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_orb.png" />
	            <button data-copytarget="#nsbstamp_orb-link">copy link</button>
	        </li>
	    </ul>
	</div>
</div>

<!-- NSB stamp - mosaic -->
<div class="griditem">
	<div>
	<img src="/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_mosaic.png" height="200px" />
	</div>

	<!-- List of links -->
	<div tabindex="0" class="onclick-menu">
	    <a href="/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_mosaic.png" download>
	    <div class="circle"><span class="fa fa-download"></span></div>
	    </a>
	</div>

	<!-- Copy embed code -->
	<div tabindex="0" class="onclick-menu">
	    <div class="circle"><span class="fa fa-link"></span></div>
	    <ul class="onclick-menu-content">
	        <li>
	            <input type="text" id="nsbstamp_mosaic-link" value="http://brand.interactivescientific.com/assets/molecules/nsbstamp/nsbstamp-png/nsbstamp_mosaic.png" />
	            <button data-copytarget="#nsbstamp_mosaic-link">copy link</button>
	        </li>
	    </ul>
	</div>
</div>


<div class="fullwidth">
<h2>NSB Stamp design file</h2>
<!-- List of links -->
<div tabindex="0" class="onclick-menu">
    <a href="/assets/molecules/nsbstamp/nsbstamp-design.ai" download>
    <div class="circle"><span class="fa fa-download"></span></div>
    </a>
</div>
</div>