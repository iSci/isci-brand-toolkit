<!-- components/molecules/slideDeck.php -->

<div class="fullwidth">
  <h2>Templates: iSci Slide Deck</h2>
  <p>Remember to download and install fonts before using templates. <a href="/atomic-core/?cat=atoms#typography">Go to Typography.</a></p>
  <!-- List of links -->
  <div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-download"></span></div>
    <ul class="onclick-menu-content">
      <li><a href="/assets/templates/slideDeck/iSci_slideDeck.potx" download>iSci Slides (POTX)</a></li>
      <li><a href="/assets/templates/documents/Nano Simbox.pptx" download>NSB Sales Deck (PPTX)</a></li>

    </ul>
  </div>
</div>