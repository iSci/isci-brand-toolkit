<!-- components/readme/manifesto.php -->
<div class="manifesto black alt">
    <h1>Refine</h1>
    <p>not Rebrand</p>
</div>
<div class="manifesto black light">
    <h1>A toolkit</h1>
    <p>not a logo</p>
</div>
<div class="manifesto black alt">
    <h1>Open design</h1>
    <p>approach</p>
</div>
<div class="manifesto black light">
    <h1>Align</h1>
    <p>with our brand purpose</p>
</div>
<div class="manifesto black alt">
    <h1>Objectivity</h1>
    <p>not subjectivity</p>
</div>
<div class="manifesto black light">
    <h1>Experience</h1>
    <p>not identity</p>
</div>
<div class="manifesto black alt">
    <h1>Changing</h1>
    <p>not controlling</p>
</div>
<div class="manifesto black light">
    <h1>Flexible</h1>
    <p>and grow with us</p>
</div>
<div class="manifesto black alt">
    <h1>Always question</h1>
    <p>this brief</p>
</div>