<!-- components/molecules/scenes.php -->

<div class="fullwidth">
  <h2>Templates: iSci scene design</h2>
  <a href="/assets/templates/scenes/isci_scenes.potx" download>
  <img src="/assets/templates/scenes/isci_scenes_example.png" width="200px"></a><br>
  <!-- List of links -->
  <div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-download"></span></div>
    <ul class="onclick-menu-content">
      <li><a href="/assets/templates/scenes/isci_scenes.potx" download>Powerpoint Template (POTX)</a></li>
      <li><a href="/assets/templates/scenes/isci_scenes.indd" download>InDesign Template (INDD)</a></li>
    </ul>
  </div>
</div>

<h2>How to make your own</h2>
<h3>Add depth with shadow</h3>
<!-- Door example plain -->
<div class="door-panel blue dark">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
<path class="door" fill="#d9d9d9" filter="url(#inset-shadow)" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg>    
</div>

<!-- Door example plain -->
<div class="door-panel purple dark">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
<path class="door" fill="#d9d9d9" filter="url(#inset-shadow)" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg>    
</div>
<p></p>
<h3>Add the background image</h3>
<!-- Door example with rainforest -->
<div class="door-panel blue dark">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
    <filter id="inset-shadow" x="-50%" y="-50%" width="200%" height="200%">
    <feComponentTransfer in=SourceAlpha>
      <feFuncA type="table" tableValues="1 0" />
    </feComponentTransfer>
    <feGaussianBlur stdDeviation="8"/>
    <feOffset dx="-8" dy="-8" result="offsetblur"/>
    <feFlood flood-color="rgb(20, 0, 0)" result="color"/>
    <feComposite in2="offsetblur" operator="in"/>
    <feComposite in2="SourceAlpha" operator="in" />
    <feMerge>
      <feMergeNode in="SourceGraphic" />
      <feMergeNode />
    </feMerge>
  </filter> 
  <defs>
  <pattern id="forest" patternUnits="userSpaceOnUse" width="2048px" height="1535px">
    <image xlink:href="/assets/background/tiles/rainforest.png" x="-500" y="0" width="1200px" height="899px" />
  </pattern>
</defs>
<path class="door" fill="url(#forest)" filter="url(#inset-shadow)" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg>    
</div>

<!-- Door example with orb -->
<div class="door-panel purple dark">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
    <filter id="inset-shadow" x="-50%" y="-50%" width="200%" height="200%">
    <feComponentTransfer in=SourceAlpha>
      <feFuncA type="table" tableValues="1 0" />
    </feComponentTransfer>
    <feGaussianBlur stdDeviation="8"/>
    <feOffset dx="-8" dy="-8" result="offsetblur"/>
    <feFlood flood-color="rgb(20, 0, 0)" result="color"/>
    <feComposite in2="offsetblur" operator="in"/>
    <feComposite in2="SourceAlpha" operator="in" />
    <feMerge>
      <feMergeNode in="SourceGraphic" />
      <feMergeNode />
    </feMerge>
  </filter> 
  <defs>
  <pattern id="orb" patternUnits="userSpaceOnUse" width="2048px" height="1535px">
    <image xlink:href="/assets/background/tiles/orb.png" x="-750" y="0" width="1200px" height="899px" />
  </pattern>
</defs>
<path class="door" fill="url(#orb)" filter="url(#inset-shadow)" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg>    
</div>

<h3>Add the foreground</h3>
<!-- Door example with rainforest -->
<div class="door-panel blue dark">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
    <filter id="inset-shadow" x="-50%" y="-50%" width="200%" height="200%">
    <feComponentTransfer in=SourceAlpha>
      <feFuncA type="table" tableValues="1 0" />
    </feComponentTransfer>
    <feGaussianBlur stdDeviation="8"/>
    <feOffset dx="-8" dy="-8" result="offsetblur"/>
    <feFlood flood-color="rgb(20, 0, 0)" result="color"/>
    <feComposite in2="offsetblur" operator="in"/>
    <feComposite in2="SourceAlpha" operator="in" />
    <feMerge>
      <feMergeNode in="SourceGraphic" />
      <feMergeNode />
    </feMerge>
  </filter> 
  <defs>
  <pattern id="forest" patternUnits="userSpaceOnUse" width="2048px" height="1535px">
    <image xlink:href="/assets/background/tiles/rainforest.png" x="-500" y="0" width="1200px" height="899px" />
  </pattern>
</defs>
<path class="door" fill="url(#forest)" filter="url(#inset-shadow)" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg> 
<div class="foreground"><img src="/assets/foreground/glucose.png">
</div>
</div>

<!-- Door example with orb -->
<div class="door-panel purple dark">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
    <filter id="inset-shadow" x="-50%" y="-50%" width="200%" height="200%">
    <feComponentTransfer in=SourceAlpha>
      <feFuncA type="table" tableValues="1 0" />
    </feComponentTransfer>
    <feGaussianBlur stdDeviation="8"/>
    <feOffset dx="-8" dy="-8" result="offsetblur"/>
    <feFlood flood-color="rgb(20, 0, 0)" result="color"/>
    <feComposite in2="offsetblur" operator="in"/>
    <feComposite in2="SourceAlpha" operator="in" />
    <feMerge>
      <feMergeNode in="SourceGraphic" />
      <feMergeNode />
    </feMerge>
  </filter> 
  <defs>
  <pattern id="orb" patternUnits="userSpaceOnUse" width="2048px" height="1535px">
    <image xlink:href="/assets/background/tiles/orb.png" x="-750" y="0" width="1200px" height="899px" />
  </pattern>
</defs>
<path class="door" fill="url(#orb)" filter="url(#inset-shadow)" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg>
<div class="foreground"><img src="/assets/foreground/glucose.png">
</div>    
</div>

<h3>Add the messaging</h3>
<!-- Door example with rainforest -->
<div class="door-panel blue dark">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
    <filter id="inset-shadow" x="-50%" y="-50%" width="200%" height="200%">
    <feComponentTransfer in=SourceAlpha>
      <feFuncA type="table" tableValues="1 0" />
    </feComponentTransfer>
    <feGaussianBlur stdDeviation="8"/>
    <feOffset dx="-8" dy="-8" result="offsetblur"/>
    <feFlood flood-color="rgb(20, 0, 0)" result="color"/>
    <feComposite in2="offsetblur" operator="in"/>
    <feComposite in2="SourceAlpha" operator="in" />
    <feMerge>
      <feMergeNode in="SourceGraphic" />
      <feMergeNode />
    </feMerge>
  </filter> 
  <defs>
  <pattern id="forest" patternUnits="userSpaceOnUse" width="2048px" height="1535px">
    <image xlink:href="/assets/background/tiles/rainforest.png" x="-500" y="0" width="1200px" height="899px" />
  </pattern>
</defs>
<path class="door" fill="url(#forest)" filter="url(#inset-shadow)" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg> 
<div class="message"><h1>Find out more about glucose</h1>
</div>
<div class="foreground"><img src="/assets/foreground/glucose.png">
</div>
</div>

<!-- Door example with orb -->
<div class="door-panel purple dark">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
    <filter id="inset-shadow" x="-50%" y="-50%" width="200%" height="200%">
    <feComponentTransfer in=SourceAlpha>
      <feFuncA type="table" tableValues="1 0" />
    </feComponentTransfer>
    <feGaussianBlur stdDeviation="8"/>
    <feOffset dx="-8" dy="-8" result="offsetblur"/>
    <feFlood flood-color="rgb(20, 0, 0)" result="color"/>
    <feComposite in2="offsetblur" operator="in"/>
    <feComposite in2="SourceAlpha" operator="in" />
    <feMerge>
      <feMergeNode in="SourceGraphic" />
      <feMergeNode />
    </feMerge>
  </filter> 
  <defs>
  <pattern id="orb" patternUnits="userSpaceOnUse" width="2048px" height="1535px">
    <image xlink:href="/assets/background/tiles/orb.png" x="-750" y="0" width="1200px" height="899px" />
  </pattern>
</defs>
<path class="door" fill="url(#orb)" filter="url(#inset-shadow)" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg>
<div class="message"><h1>Explore <br>Nano <br>Simbox</h1>
</div>
<div class="foreground"><img src="/assets/foreground/glucose.png">
</div>    
</div>

<h3>Animate</h3>
<!-- Door example with rainforest -->
<div class="door-panel blue dark">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
    <filter id="inset-shadow" x="-50%" y="-50%" width="200%" height="200%">
    <feComponentTransfer in=SourceAlpha>
      <feFuncA type="table" tableValues="1 0" />
    </feComponentTransfer>
    <feGaussianBlur stdDeviation="8"/>
    <feOffset dx="-8" dy="-8" result="offsetblur"/>
    <feFlood flood-color="rgb(20, 0, 0)" result="color"/>
    <feComposite in2="offsetblur" operator="in"/>
    <feComposite in2="SourceAlpha" operator="in" />
    <feMerge>
      <feMergeNode in="SourceGraphic" />
      <feMergeNode />
    </feMerge>
  </filter> 
  <defs>
  <pattern id="forest" patternUnits="userSpaceOnUse" width="2048px" height="1535px">
    <image xlink:href="/assets/background/tiles/rainforest.png" x="-500" y="0" width="1200px" height="899px" />
  </pattern>
</defs>
<path class="door" fill="url(#forest)" filter="url(#inset-shadow)" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg> 
<div class="message"><h1>Find out more about glucose</h1>
</div>
<div class="foreground"><img class="rotate" src="/assets/foreground/glucose.png">
</div>
</div>

<!-- Door example with orb -->
<div class="door-panel purple dark">
<svg class="door-icon"version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 205.4 609" style="enable-background:new 0 0 205.4 609;" xml:space="preserve">
    <filter id="inset-shadow" x="-50%" y="-50%" width="200%" height="200%">
    <feComponentTransfer in=SourceAlpha>
      <feFuncA type="table" tableValues="1 0" />
    </feComponentTransfer>
    <feGaussianBlur stdDeviation="8"/>
    <feOffset dx="-8" dy="-8" result="offsetblur"/>
    <feFlood flood-color="rgb(20, 0, 0)" result="color"/>
    <feComposite in2="offsetblur" operator="in"/>
    <feComposite in2="SourceAlpha" operator="in" />
    <feMerge>
      <feMergeNode in="SourceGraphic" />
      <feMergeNode />
    </feMerge>
  </filter> 
  <defs>
  <pattern id="orb" patternUnits="userSpaceOnUse" width="2048px" height="1535px">
    <image xlink:href="/assets/background/tiles/orb.png" x="-750" y="0" width="1200px" height="899px" />
  </pattern>
</defs>
<path class="door" fill="url(#orb)" filter="url(#inset-shadow)" d="M205.4,500.5L0,609V0l205.4,108.4V500.5z"/>
</svg>
<div class="message"><h1>Explore <br>Nano <br>Simbox</h1>
</div>
<div class="foreground"><img class="grow" src="/assets/foreground/glucose.png">
</div>    
</div>




