<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <base href="../">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Interactive Scientific Brand Toolkit</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" type="text/css" href="atomic-core/css/site.css">
    
    <link rel="stylesheet" type="text/css" href="css/main.css">

    <link rel="stylesheet" href="atomic-core/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.min.css">

        <link rel="apple-touch-icon" sizes="180x180" href="atomic-core/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="atomic-core/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="atomic-core/favicon/favicon-16x16.png">
    <link rel="manifest" href="atomic-core/favicon/site.webmanifest">
    <link rel="mask-icon" href="atomic-coreatomic-core/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="atomic-core/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#2d89ef">
      <meta name="msapplication-config" content="atomic-core/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto:400,700|Source+Sans+Pro:400,700|Source+Serif+Pro:400,600,700" rel="stylesheet">

    <?php
        $filename = '../atomic-head.php';
        if (file_exists($filename)) {
            include("../atomic-head.php");
        }
    ?>
    
    <?php
    
        $parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );

        $file_name =  $parse_uri[0] . 'wp-load.php';

        if (file_exists($file_name)) {
        require_once( $parse_uri[0] . 'wp-load.php' );
        }

    ?>

    


</head>
