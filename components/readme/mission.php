<!-- components/readme/mission.php -->
<div class="brand ambition" id="grad-purple-dark">
<h2>If we’re going to deal with the world’s biggest challenges, like climate change and health, more of us need to embrace science now. We all need to work together.</h2>
<h4>The world needs a huge, diverse, and creative pool of scientists who can work collaboratively and communicate clearly so that all of us understand what needs to be done.</h4>

<h4>We believe that if we can equip people with immersive tools and technology that make the invisible, molecular world visible, and make collaboration beautifully simple, we’ll make big strides where we need them most. That’s what iSci does.</h4>

<h4>Change the way we think about science – make it about curiosity, creativity and collaboration – and anyone can be a scientist. Get this fantastically diverse and motivated tribe of scientifically literate people working together, and the sky’s the limit when it comes to what we can achieve.</h4>

<h4>That’s what we want. By making the invisible world visible to everyone, we are striving to help today’s scientists work more creatively and collaborate. And using the same technology, we want to inspire and teach the next generation of children to see the world differently and become the scientists our future needs.</h4>
</div>