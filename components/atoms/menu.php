<!-- components/atoms/menu.php -->

<!-- List of links -->
<div tabindex="0" class="onclick-menu">
    <div class="circle"><span class="fa fa-download"></span></div>
    <ul class="onclick-menu-content">
        <li><a href="assets/logo.svg" download>Logo SVG</a></li>
        <li><a href="assets/logo-small.svg" download>Small Logo SVG</a></li>
    </ul>
</div>

<!-- Copy embed code -->
<div tabindex="0" class="onclick-menu">
            <div class="circle"><span class="fa fa-link"></span></div>
            <ul class="onclick-menu-content">
                <li><input type="text" value="http://brand.interactivescientific.com/assets/logo.svg" id="embedLink"><button onclick="copyLink('embedLink')">Copy link</button></li>
            </ul>
        </div>